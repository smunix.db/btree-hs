{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeFamilies #-}

-- |
module Data.BTree.BTree where

import Control.Monad
import Data.BTree.Array
import qualified Data.BTree.Array as Array
import Data.BTree.Degree
import Data.BTree.Store
import qualified Data.BTree.Utils as Utils
import Data.Data (Proxy (Proxy))
import Data.Int
import Data.Word
import Foreign (Ptr, castPtr, mallocBytes, plusPtr)
import GHC.TypeLits
import Optics

instance (KnownNat d) => Degree (BTree k v d) where
  degree = fromInteger . natVal $ Proxy @d

data Ty = LeafTy | NodeTy

instance Store Ty where
  sizeOf = sizeOf @Int32
  sizeOf' _ = sizeOf' @Int32 0
  alignOf = alignOf @Int32
  peekFrom = peekFrom @Int32 . castPtr >=> return . i32Ty
  peekFromN (castPtr -> ptr) n = i32Ty <$> peekFromN ptr n
  pokeInto (castPtr -> ptr) (tyi32 -> i32) = pokeInto ptr i32
  pokeIntoN (castPtr -> ptr) n (tyi32 -> i32) = pokeIntoN ptr n i32

tyi32 :: Ty -> Int32
tyi32 LeafTy = 0x7EAF
tyi32 NodeTy = 0x90DE

i32Ty :: Int32 -> Ty
i32Ty 0x7EAF = LeafTy
i32Ty 0x90DE = NodeTy
i32Ty _ = error "not supported"

data BTree k v (d :: Nat) where
  Leaf :: CArray (k, v) d -> BTree k v d
  Node :: (PBTree k v d, CArray (k, PBTree k v d) d) -> BTree k v d

newtype PBTree k v d = PBTree {_pbtree :: Ptr (BTree k v d)}

makeLenses ''PBTree

instance Store (PBTree k v d) where
  sizeOf = sizeOf @(Ptr (BTree k v d))
  sizeOf' (castPtr . _pbtree -> ptr) = sizeOf' ptr
  alignOf = alignOf @(Ptr (BTree k v d))
  peekFrom (castPtr -> ptr) = PBTree <$> peekFrom ptr
  peekFromN (castPtr -> ptr) n = PBTree <$> peekFromN ptr n
  pokeInto (castPtr -> ptr) (view pbtree -> pbt) = pokeInto ptr pbt
  pokeIntoN (castPtr -> ptr) n (view pbtree -> pbt) = pokeIntoN ptr n pbt

instance (Store k, Store v) => Store (BTree k v d) where
  sizeOf = undefined
  sizeOf' = undefined
  alignOf = alignOf @Word64
  peekFrom (castPtr -> ptr@((`plusPtr` (sizeOf @Ty)) -> pload)) =
    peekFrom ptr >>= \case
      LeafTy -> return . Leaf $ array (castPtr pload)
      NodeTy -> Node <$> peekFrom (castPtr pload)

  peekFromN ptr n = undefined

  pokeInto ptr@((`plusPtr` sizeOf @Ty) -> payload) (Leaf carr) = do
    pokeInto (castPtr ptr) LeafTy
    pokeInto payload carr
  pokeInto ptr@((`plusPtr` sizeOf @Ty) -> payload) (Node p) = do
    pokeInto (castPtr ptr) NodeTy
    pokeInto payload p

  pokeIntoN = undefined

btree ::
  (Store k, Store v) =>
  (CArray (k, v) d -> IO r) ->
  ((PBTree k v d, CArray (k, PBTree k v d) d) -> IO r) ->
  PBTree k v d ->
  IO r
btree lfn nfn (_pbtree -> pbtr) = do
  peekFrom pbtr >>= \case
    Leaf (lfn -> io) -> io
    Node (nfn -> io) -> io

{- Create an empty BTree (space is allocated) -}
newLeaf ::
  forall k v d r.
  (KnownNat d, Store k, Store v, Store (k, v)) =>
  (CArray (k, v) d -> IO r) ->
  IO (r, PBTree k v d)
newLeaf init = do
  bytes <- mallocBytes sz
  pokeInto (castPtr bytes) LeafTy
  let (array -> carr) = bytes `plusPtr` (sizeOf @Ty)
  setSize carr 0
  r <- init carr
  return (r, PBTree bytes)
  where
    sz =
      sizeOf @Ty
        + sizeOf @Int32
        + alignTo
          ( capacity @(BTree k v d)
              * (sizeOf @(k, v))
          )
          (alignOf @(k, v))

{- Create a new BTree node -}
newNode ::
  forall k v d.
  (Store k, KnownNat d) =>
  (CArray (k, PBTree k v d) d -> IO (k, PBTree k v d)) ->
  IO (k, PBTree k v d)
newNode init = do
  bytes <- mallocBytes sz
  pokeInto (castPtr bytes) NodeTy
  let (array -> carr) = bytes `plusPtr` (sizeOf @Ty)
  setSize carr 0
  (k, pbt) <- init
  where
    carr
      sz =
        sizeOf @Ty
          + sizeOf @Int32
          + alignTo
            ( capacity @(CArray (k, PBTree k v d) d)
                * (sizeOf @(k, PBTree k v d))
            )
            (alignOf @(k, PBTree k v d))

insert ::
  forall k v d.
  (Ord k, Store k, Store v, KnownNat d) =>
  k ->
  v ->
  PBTree k v d ->
  IO (PBTree k v d)
insert k v pbt = do
  insert' k v pbt >>= maybe (pure pbt) (uncurry mkNode)
  where
    mkNode :: k -> PBTree k v d -> IO (PBTree k v d)
    mkNode k' pbt' = do
      (k'', pbt'') <- newNode @k @v @d \carr'@(payload -> pload') -> do
        carr' `setSize` 1
        pokeInto pload' (k', pbt')
        peekFrom pload'
      error "not implemented"
      where
        sz = undefined
    {-
        mkNode :: k -> BTree k v d -> IO (BTree k v d)
        mkNode k' bt' = do
          carr@(getData -> ptr) <- array <$> mallocBytes sz
          pokeInto ptr (k', _a)
          carr `setSize` 1
          return $ Node (bt, carr)
          where
            sz = sizeOf @Int + alignTo (capacity @(BTree k v d) * (sizeOf @(k, PBTree k v d))) (alignOf @(k, PBTree k v d))
    -}

    {- A boundary value @k@ and values @v@, such that @k <= v@ -}
    insert' :: k -> v -> PBTree k v d -> IO (Maybe (k, PBTree k v d))
    insert' k v = btree insLeaf insNode
      where
        insNode :: (PBTree k v d, CArray (k, PBTree k v d) d) -> IO (Maybe (k, PBTree k v d))
        insNode (hpbt, carr) = do
          sz <- getSize carr
          if
              | sz == 0 -> insert' k v hpbt >>= insSplit sz
              | otherwise -> do
                (i, _) <- lower carr (compare k . fst)
                error "not implemented"
          where
            insSplit :: Int -> Maybe (k, PBTree k v d) -> IO (Maybe (k, PBTree k v d))
            insSplit sz Nothing = return Nothing
            insSplit sz (Just (k', pbt)) = Utils.insertSplit k' pbt splitFn carr
              where
                splitFn :: CArray (k, PBTree k v d) d -> Int -> Int -> IO (k, PBTree k v d)
                splitFn (payload -> pload) i e = newNode @k @v @d \carr'@(payload -> pload') -> do
                  move pload' (offset pload (i + 1)) (offset pload e)
                  carr' `setSize` (e - i - 1)
                  peekFromN pload i

        insLeaf :: CArray (k, v) d -> IO (Maybe (k, PBTree k v d))
        insLeaf = Utils.insertSplit k v splitFn
          where
            splitFn :: CArray (k, v) d -> Int -> Int -> IO (k, PBTree k v d)
            splitFn (payload -> pload) i e = newLeaf @k @v @d \carr'@(payload -> pload') -> do
              move pload' (offset pload i) (offset pload e)
              carr' `setSize` (e - i)
              fst <$> peekFrom pload'
