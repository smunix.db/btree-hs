{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DefaultSignatures #-}

-- |
module Data.BTree.Store where

import Data.ByteString.Internal (c2w, w2c)
import Data.Char (ord)
import Data.Function (on)
import Data.Int
import Data.Word
import Foreign (Ptr, Storable, castPtr, intPtrToPtr, mallocBytes, plusPtr, ptrToIntPtr)
import qualified Foreign as F

alignTo :: forall a. Integral a => a -> a -> a
alignTo x a
  | r == 0 = x
  | otherwise = a * (1 + q)
  where
    (q, r) = x `divMod` a

-- >>> (\(a,x) -> ("alignTo(" <> show x <> ", "<> show a <> ")=", (alignTo x a))) <$> [(a,x)|a<-[1..5], x<-[1..5]]
-- [("alignTo(1, 1)=",1),("alignTo(2, 1)=",2),("alignTo(3, 1)=",3),("alignTo(4, 1)=",4),("alignTo(5, 1)=",5),("alignTo(1, 2)=",2),("alignTo(2, 2)=",2),("alignTo(3, 2)=",4),("alignTo(4, 2)=",4),("alignTo(5, 2)=",6),("alignTo(1, 3)=",3),("alignTo(2, 3)=",3),("alignTo(3, 3)=",3),("alignTo(4, 3)=",6),("alignTo(5, 3)=",6),("alignTo(1, 4)=",4),("alignTo(2, 4)=",4),("alignTo(3, 4)=",4),("alignTo(4, 4)=",4),("alignTo(5, 4)=",8),("alignTo(1, 5)=",5),("alignTo(2, 5)=",5),("alignTo(3, 5)=",5),("alignTo(4, 5)=",5),("alignTo(5, 5)=",5)]

{- How to store data of type @a@ -}
class Store a where
  sizeOf :: Int
  default sizeOf :: (Storable a) => Int
  sizeOf = F.sizeOf @a undefined

  sizeOf' :: a -> IO Int
  default sizeOf' :: (Storable a) => a -> IO Int
  sizeOf' = pure . F.sizeOf

  alignOf :: Int
  default alignOf :: (Storable a) => Int
  alignOf = F.alignment @a undefined

  peekFrom :: Ptr a -> IO a
  default peekFrom :: (Storable a) => Ptr a -> IO a
  peekFrom = F.peek

  peekFromN :: Ptr a -> Int -> IO a
  default peekFromN :: (Storable a) => Ptr a -> Int -> IO a
  peekFromN = F.peekElemOff

  pokeInto :: Ptr a -> a -> IO ()
  default pokeInto :: (Storable a) => Ptr a -> a -> IO ()
  pokeInto = F.poke

  pokeIntoN :: Ptr a -> Int -> a -> IO ()
  default pokeIntoN :: (Storable a) => Ptr a -> Int -> a -> IO ()
  pokeIntoN = F.pokeElemOff

{- Ptr at a given offset (in count of elements) -}
offset :: forall a. (Store a) => Ptr a -> Int -> Ptr a
offset ptr i = ptr `plusPtr` (i * (sizeOf @a))

{- Memove-like operation (from @i@ to @d@, until we hit @e@) -}
move :: forall a. (Store a) => Ptr a -> Ptr a -> Ptr a -> IO ()
move d i ((== i) -> True) = return ()
move d@(flip offset 1 -> d') i@(flip offset 1 -> i') e = do peekFrom i >>= pokeInto d; move d' i' e

{- Store pairs if we can store the constituents -}
instance (Store a, Store b) => Store (a, b) where
  sizeOf = alignTo (sizeOf @a) (alignOf @b) + sizeOf @b
  sizeOf' (a, b) = (\a' b' -> alignTo a' (alignOf @b) + b') <$> sizeOf' a <*> sizeOf' b
  alignOf = max (alignOf @a) (alignOf @b)
  peekFrom ptr = do
    a <- peekFrom (castPtr ptr)
    b <- peekFrom (intPtrToPtr $ alignTo (ptrToIntPtr ptr) (fromIntegral $ alignOf @b))
    return (a, b)
  peekFromN ptr n = peekFrom . intPtrToPtr $ alignTo (ptrToIntPtr ptr) (fromIntegral $ alignOf @(a, b)) + fromIntegral (n * sizeOf @(a, b))
  pokeInto ptr (a, b) = do
    pokeInto (castPtr ptr) a
    pokeInto (intPtrToPtr $ alignTo (ptrToIntPtr ptr) (fromIntegral $ alignOf @b)) b
  pokeIntoN ptr n = pokeInto (intPtrToPtr $ alignTo (ptrToIntPtr ptr) (fromIntegral $ alignOf @(a, b)) + fromIntegral (n * sizeOf @(a, b)))

instance Store Word8

instance Store Int8

instance Store Word16

instance Store Int16

instance Store Word32

instance Store Int32

instance Store Float

instance Store Word64

instance Store Int64

instance Store Double

instance Store (Ptr a)

instance (sTy ~ Char, dTy ~ Int8) => Store Char where
  sizeOf = sizeOf @dTy
  sizeOf' _ = sizeOf' @dTy undefined
  alignOf = alignOf @dTy
  peekFrom (castPtr @sTy @dTy -> ptr) = w2c . fromIntegral <$> peekFrom ptr
  peekFromN ptr n = peekFrom . intPtrToPtr $ alignTo (ptrToIntPtr ptr) (fromIntegral $ alignOf @sTy) + fromIntegral (n * sizeOf @sTy)
  pokeInto (castPtr -> ptr) (fromIntegral . c2w -> w8) = pokeInto @dTy ptr w8
  pokeIntoN ptr n = pokeInto (intPtrToPtr $ alignTo (ptrToIntPtr ptr) (fromIntegral $ alignOf @sTy) + fromIntegral (n * sizeOf @sTy))

instance (sTy ~ Int, dTy ~ Int32) => Store Int where
  sizeOf = sizeOf @dTy
  sizeOf' = sizeOf' @dTy . fromIntegral
  alignOf = alignOf @dTy
  peekFrom (castPtr @sTy @dTy -> ptr) = fromIntegral <$> peekFrom ptr
  peekFromN ptr n = peekFrom . intPtrToPtr $ alignTo (ptrToIntPtr ptr) (fromIntegral $ alignOf @sTy) + fromIntegral (n * sizeOf @sTy)
  pokeInto (castPtr -> ptr) (fromIntegral -> w32) = pokeInto @dTy ptr w32
  pokeIntoN ptr n = pokeInto (intPtrToPtr $ alignTo (ptrToIntPtr ptr) (fromIntegral $ alignOf @sTy) + fromIntegral (n * sizeOf @sTy))

-- >>> sizeOf @Int
-- 4

-- >>> sizeOf' @Int 1
-- 4

-- >>> sizeOf @Char
-- 1

-- >>> sizeOf @Float
-- 4

-- >>> sizeOf' @Float 1
-- 4

-- >>> sizeOf @Double
-- 8

-- >>> sizeOf @(Int,Char)
-- 5

-- >>> sizeOf @(Char,Int)
-- 8

-- >>> alignOf @Int 1
-- 4

-- >>> sizeOf @Word32 1
-- 4

-- >>> alignOf  @Word32 1
-- 4

-- >>> sizeOf @Char
-- 1

-- >>> alignOf @Char
-- 1

-- >>> sizeOf @Int32
-- 4
