-- |
module Data.BTree.Utils where

import Data.BTree.Array
import Data.BTree.Degree
import Data.BTree.Store (Store (peekFrom, peekFromN, pokeInto, pokeIntoN, sizeOf), offset)
import Data.Functor (($>))
import Foreign (Ptr, plusPtr)
import GHC.TypeLits (KnownNat)

insertSplit ::
  forall k v d r.
  (Ord k, Store k, Store v, KnownNat d) =>
  k ->
  v ->
  (CArray (k, v) d -> Int -> Int -> IO r) ->
  CArray (k, v) d ->
  IO (Maybe r)
insertSplit k v splitFn carr = do
  (i, kv') <- lower carr (compare k . fst)
  sz <- getSize carr
  if
      | i == sz -> insertAt k v splitFn i carr
      | Just (_, _) <- kv' -> insertAt k v splitFn i carr
      | otherwise -> put carr i (k, v) $> Nothing

{- Insert in place and split if necessary -}
insertAt ::
  forall k v d r carr a.
  (KnownNat d, Store k, Store v, a ~ (k, v), carr ~ CArray a d) =>
  k ->
  v ->
  (carr -> Int -> Int -> IO r) ->
  Int ->
  carr ->
  IO (Maybe r)
insertAt k v splitFn i carr@(payload -> ptr) = do
  sz <- getSize carr
  pushDown (offset ptr sz)
  setSize carr (sz + 1) -- increment size
  pokeIntoN ptr i (k, v)
  if sz < capacity @carr
    then return Nothing
    else do
      -- at capacity, split
      let h = 1 + degree @carr
      r <- splitFn carr (h + 1) sz
      setSize carr h
      return $ Just r
  where
    {- TODO: use Foreign.Marshall.Utils.moveBytes -}
    pushDown :: Ptr a -> IO ()
    pushDown e = move (offset ptr i)
      where
        move :: Ptr a -> IO ()
        move ((== e) -> True) = return ()
        move i = move i' >> peekFrom @a i >>= pokeInto i'
          where
            i' = i `offset` 1
