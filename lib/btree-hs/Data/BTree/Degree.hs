{-# LANGUAGE AllowAmbiguousTypes #-}

-- |
module Data.BTree.Degree where

class Degree a where
  degree :: Int
  capacity :: Int
  capacity = 2 * degree @a
