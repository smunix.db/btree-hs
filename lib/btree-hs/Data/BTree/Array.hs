{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE TupleSections #-}

-- |
module Data.BTree.Array where

import Data.BTree.Degree
import Data.BTree.Store
import Data.Data (Proxy (Proxy))
import Foreign (Ptr, castPtr, intPtrToPtr, mallocBytes, newArray, plusPtr, ptrToIntPtr)
import GHC.TypeLits
import Optics

newtype CArray a (d :: Nat) = CArray {_carray :: Ptr a}

makeLenses ''CArray

instance Store (CArray a d) where
  sizeOf = sizeOf @(Ptr a)
  sizeOf' (CArray ptr) = sizeOf' ptr
  alignOf = alignOf @(Ptr a)
  peekFrom (castPtr -> ptr) = CArray <$> peekFrom ptr
  peekFromN (castPtr -> ptr) n = CArray <$> peekFromN ptr n
  pokeInto (castPtr -> ptr) (_carray -> ptr') = pokeInto ptr ptr'
  pokeIntoN (castPtr -> ptr) n (_carray -> ptr') = pokeIntoN ptr n ptr'

instance (KnownNat d) => Degree (CArray a d) where
  degree = fromInteger . natVal $ Proxy @d

class Payload a e | a -> e where
  payload :: a -> Ptr e

instance (Store a) => Payload (CArray a d) a where
  payload (_carray -> ptr) = intPtrToPtr $ alignTo (ptrToIntPtr $ ptr `plusPtr` sizeOf @Int) (fromIntegral $ alignOf @a)

class GetSize a where
  getSize :: a -> IO Int

instance GetSize (CArray a d) where
  getSize (CArray ptr) = peekFrom $ castPtr ptr

class SetSize a where
  setSize :: a -> Int -> IO ()

instance SetSize (CArray a d) where
  setSize (CArray ptr) = pokeInto $ castPtr ptr

class Lower arr a | arr -> a where
  lower :: arr -> (a -> Ordering) -> IO (Int, Maybe a)
  default lower :: (GetSize arr) => arr -> (a -> Ordering) -> IO (Int, Maybe a)
  lower arr cmp = getSize arr >>= lower' arr cmp 0
  lower' :: arr -> (a -> Ordering) -> Int -> Int -> IO (Int, Maybe a)

instance (Store a) => Lower (CArray a d) a where
  lower' carr@(payload -> ptr) cmp = go
    where
      go :: Int -> Int -> IO (Int, Maybe a)
      go !i !sz
        | sz <= 0 = return (i, Nothing)
        | otherwise = do
          v@(cmp -> o) <- peekFromN ptr ii
          case o of
            LT -> go i h
            EQ -> return (ii, Just v)
            GT -> go (ii + 1) (sz - (h + 1))
        where
          h = sz `div` 2
          ii = i + h

-- >>> do { carr <- array @(CArray Int 5) <$> mallocBytes  (6 * sizeOf @Int); setSize carr 5; mapM (\i -> pokeIntoN (payload carr) i i) [0..4::Int]; mapM (\i -> peekFromN (payload carr) i) [0..4::Int];  }
-- [0,1,2,3,4]

-- >>> do { carr <- array @(CArray Int 5) <$> mallocBytes  (6 * sizeOf @Int); setSize carr 5; mapM (\i -> w carr i (i+1)) [0..4::Int]; mapM (\i -> (i,) <$> lower carr (i `compare`)) [-2..8::Int] }
-- [(-2,(0,Nothing)),(-1,(0,Nothing)),(0,(0,Nothing)),(1,(0,Just 1)),(2,(1,Just 2)),(3,(2,Just 3)),(4,(3,Just 4)),(5,(4,Just 5)),(6,(5,Nothing)),(7,(5,Nothing)),(8,(5,Nothing))]

-- >>> do { carr <- array @(CArray Int 5) <$> mallocBytes  (7 * sizeOf @Int); setSize carr 6; mapM (\i -> w carr i (i+1)) [0..5::Int]; mapM (\i -> (i,) <$> lower carr (i `compare`)) [-2..8::Int] }
-- [(-2,(0,Nothing)),(-1,(0,Nothing)),(0,(0,Nothing)),(1,(0,Just 1)),(2,(1,Just 2)),(3,(2,Just 3)),(4,(3,Just 4)),(5,(4,Just 5)),(6,(5,Just 6)),(7,(6,Nothing)),(8,(6,Nothing))]

-- >>> do { carr <- array @(CArray Int 5) <$> mallocBytes  (6 * sizeOf @Int); setSize carr 0; mapM (\i -> w carr i (i+1)) [0..4::Int]; mapM (\i -> (i,) <$> lower carr (i `compare`)) [-2..8::Int] }
-- [(-2,(0,Nothing)),(-1,(0,Nothing)),(0,(0,Nothing)),(1,(0,Nothing)),(2,(0,Nothing)),(3,(0,Nothing)),(4,(0,Nothing)),(5,(0,Nothing)),(6,(0,Nothing)),(7,(0,Nothing)),(8,(0,Nothing))]

-- >>> do { carr <- array @(CArray Int 5) <$> mallocBytes  (6 * sizeOf @Int); setSize carr 1; mapM (\i -> w carr i (i+1)) [0..4::Int]; mapM (\i -> (i,) <$> lower carr (i `compare`)) [-2..8::Int] }
-- [(-2,(0,Nothing)),(-1,(0,Nothing)),(0,(0,Nothing)),(1,(0,Just 1)),(2,(1,Nothing)),(3,(1,Nothing)),(4,(1,Nothing)),(5,(1,Nothing)),(6,(1,Nothing)),(7,(1,Nothing)),(8,(1,Nothing))]

class Access a e | a -> e where
  put :: a -> Int -> e -> IO ()
  get :: a -> Int -> IO (Maybe e)

instance (Store a) => Access (CArray a d) a where
  put carr n a =
    getSize carr >>= \sz ->
      if
          | n < 0 -> return ()
          | n < sz -> pokeIntoN (payload carr) n a
          | otherwise -> return ()
  get carr n =
    getSize carr >>= \sz ->
      if
          | n < 0 -> return Nothing
          | n < sz -> Just <$> peekFromN (payload carr) n
          | otherwise -> return Nothing

class Array arr e | arr -> e where
  array :: Ptr e -> arr

instance (Store a) => Array (CArray a d) a where
  array = CArray . castPtr
