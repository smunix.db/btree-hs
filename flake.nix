{
  description = "Various DB storage implementations in Haskell";
  inputs = {
    np.url = "github:nixos/nixpkgs?ref=haskell-updates";
    nf.url = "github:numtide/nix-filter?ref=master";
    fu.url = "github:numtide/flake-utils?ref=master";
    ds.url = "github:numtide/devshell?ref=master";
    optc = {
      url = "github:well-typed/optics?ref=master";
      flake = false;
    };
  };
  outputs = { self, np, nf, fu, ds, optc }:
    with np.lib;
    with nf.lib;
    with fu.lib;
    eachSystem [ "x86_64-linux" ] (system:
      let
        version =
          "${substring 0 9 self.lastModifiedDate}.${self.shortRev or "dirty"}";
        config = { };
        overlay = final: _:
          with final;
          with haskellPackages.extend (final: _:
            with final;
            with haskell.lib; rec {
              optics-core = dontHaddock (disableLibraryProfiling
                (callCabal2nix "optics-core" ("${optc}/optics-core") { }));
              optics-extra = dontHaddock (disableLibraryProfiling
                (callCabal2nix "optics-extra" ("${optc}/optics-extra") { }));
              optics-th = dontHaddock (disableLibraryProfiling
                (callCabal2nix "optics-th" ("${optc}/optics-th") { }));
              optics = dontHaddock (disableLibraryProfiling
                (callCabal2nix "optics" ("${optc}/optics") {
                  inherit optics-core optics-extra optics-th;
                }));
            });
          with haskell.lib; {
            db-tree = (disableLibraryProfiling (callCabal2nix "db-tree"
              (filter {
                root = ./.;
                exclude = [ (matchExt "cabal") ];
              }) { })).overrideAttrs (o: {
                version = "${o.version}.${version}";
                enableParallelBuilding = true;
              });
          };
        overlays = [ overlay ds.overlay ];
        pkgs = (import np { inherit system config overlays; });
      in rec {
        inherit overlay;
        packages = flattenTree (recurseIntoAttrs { inherit (pkgs) db-tree; });
        defaultPackage = packages.db-tree;
        devShell = with pkgs;
          devshell.mkShell {
            name = "DB";
            packages = [ db-tree ];
          };
      });
}
